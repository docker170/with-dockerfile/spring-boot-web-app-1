# Spring Boot Web Application in Docker Container
This app can be run on a standalone or in docker container. 

## How to run this application
```
s1 -  Running within host
   - Bootstap the app
   
s2 - Running as DC
     -- mvn clean install
     -- docker build -t my-sb-docker .
     -- docker run -p 8080:8085 -e "JAVA_OPTS=-Dserver.port=8085 -Dserver.servlet.context-path=/app" my-sb-docker-correto:v1  
     -- http://127.0.0.1:8080/app/products
```