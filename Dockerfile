# base image for spring boot app
FROM centos:8.3.2011

# installing java within  base image docker container
# [WORKING]
# RUN yum install -y java-11-openjdk-devel
#installing java 11 correto version
# [WORKING]
RUN rpm --import https://yum.corretto.aws/corretto.key && \
  curl -L -o /etc/yum.repos.d/corretto.repo https://yum.corretto.aws/corretto.repo && \
  yum install -y java-11-amazon-corretto-devel


RUN echo "Installed java version " && \
  java -version

# this dir is used by sprint boot app to store temp files
# this is mandatory for embedded tomcat server
VOLUME /tmp

# to move atrifact from host to DI
# ADD host-source DC-destination
ADD /target/spring-boot-web-0.0.1-SNAPSHOT.jar myapp.jar

# this update timestamp of artifact within DI
RUN sh -c 'touch /myapp.jar'

# Docker use ENTRYPOINT to run app in DC
# ENTRYPOINT ["str1", "str2", str3] = "str1 str2 str3"
# In our case the comman will run as "java <JAVA_OPTS comes as part of docker run> -jar /myapp.jar"
# "sh", "-c" tell docker to run "java ${JAVA_OPTS} -jar /myapp.jar" as shell scipt 
ENTRYPOINT ["sh", "-c", "java ${JAVA_OPTS} -jar /myapp.jar"]
